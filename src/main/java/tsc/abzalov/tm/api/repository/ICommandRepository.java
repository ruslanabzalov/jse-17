package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<String> getCommandNames();

    Collection<String> getCommandArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getArgumentByName(String name);

    void add(AbstractCommand command);

}
