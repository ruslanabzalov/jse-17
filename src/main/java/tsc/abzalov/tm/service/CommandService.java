package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.IncorrectCommandException;

import java.util.Collection;

import static org.apache.commons.lang3.StringUtils.isBlank;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getCommandArguments() {
        return commandRepository.getCommandArguments();
    }

    @Override
    public AbstractCommand getCommandByName(@NotNull String name) throws IncorrectCommandException {
        if (isBlank(name)) throw new IncorrectCommandException(name);
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getArgumentByName(@NotNull String name) throws IncorrectCommandException {
        if (isBlank(name)) throw new IncorrectCommandException(name);
        return commandRepository.getArgumentByName(name);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
