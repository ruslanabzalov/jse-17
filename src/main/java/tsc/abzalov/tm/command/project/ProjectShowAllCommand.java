package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

public class ProjectShowAllCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-all-projects";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL PROJECTS LIST\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final List<Project> projects = projectService.findAllProjects();
            projects.forEach(project -> System.out.println((projectService.indexOf(project) + 1) + ". " + project));
            System.out.println();
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
