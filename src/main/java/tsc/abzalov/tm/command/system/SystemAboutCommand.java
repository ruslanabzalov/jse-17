package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemAboutCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "about";
    }

    @Override
    public String getCommandArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Shows developer info";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("Developer Full Name: Ruslan Abzalov");
        System.out.println("Developer Email: rabzalov@tsconsulting.com\n");
    }

}
