package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "update-project-by-index";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by index.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT PROJECT BY INDEX\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final int projectIndex = inputIndex();
            final String projectName = inputName();
            final String projectDescription = inputDescription();
            System.out.println();

            final Project project = projectService.updateProjectByIndex(projectIndex, projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }

            System.out.println("Project was successfully updated.");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
