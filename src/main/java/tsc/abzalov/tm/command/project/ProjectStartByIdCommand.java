package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public class ProjectStartByIdCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "start-project-by-id";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("START PROJECT BY ID\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final Project project = projectService.startProjectById(inputId());
            if (project == null) {
                System.out.println(
                        "Project was not started! Please, check that project exists or it has correct status.\n"
                );
                return;
            }

            System.out.println("Project was successfully started.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
