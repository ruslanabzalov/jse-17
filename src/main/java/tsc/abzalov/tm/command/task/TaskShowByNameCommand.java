package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public class TaskShowByNameCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-task-by-name";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by name.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("FIND TASK BY NAME\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            final String taskName = inputName();
            System.out.println();

            final Task task = taskService.findTaskByName(taskName);
            if (task == null) {
                System.out.println("Searched task was not found.\n");
                return;
            }

            System.out.println((taskService.indexOf(task) + 1) + ". " + task + "\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
