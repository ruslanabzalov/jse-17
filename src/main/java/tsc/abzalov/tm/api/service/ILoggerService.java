package tsc.abzalov.tm.api.service;

public interface ILoggerService {

    void info(String message);

    void command(String cmd);

    void error(Exception exception);

}
