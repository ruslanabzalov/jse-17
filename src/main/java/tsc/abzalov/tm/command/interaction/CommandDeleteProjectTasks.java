package tsc.abzalov.tm.command.interaction;

import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public class CommandDeleteProjectTasks extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-project-with-tasks";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete project with tasks.";
    }

    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE PROJECTS WITH TASKS\n");

        final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        if (projectTasksService.hasData()) {
            System.out.println("Project");
            String projectId = InputUtil.inputId();
            System.out.println();

            if (projectTasksService.findProjectById(projectId) == null) {
                System.out.println("Project was not found.\n");
                return;
            }

            projectTasksService.deleteProjectTasksById(projectId);
            projectTasksService.deleteProjectById(projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }

        System.out.println("One of the lists is empty!");
    }

}
