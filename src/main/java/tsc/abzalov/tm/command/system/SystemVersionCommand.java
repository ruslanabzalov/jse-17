package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemVersionCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "version";
    }

    @Override
    public String getCommandArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Shows application version.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("Version: 1.0.0\n");
    }

}
