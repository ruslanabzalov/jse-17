package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.*;
import static tsc.abzalov.tm.enumeration.Status.TODO;
import static tsc.abzalov.tm.util.Formatter.DATE_TIME_FORMATTER;

public final class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Status status = TODO;

    @Nullable
    private String projectId;

    @Nullable
    private LocalDateTime startDate;

    @Nullable
    private LocalDateTime endDate;

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = isBlank(description) ? DEFAULT_DESCRIPTION : description;
    }

    @NotNull
    public String getId() {
        return this.id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable String projectId) {
        this.projectId = projectId;
    }

    @Nullable
    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable LocalDateTime startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(@Nullable LocalDateTime endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        final String correctProjectId = (this.projectId == null) ? DEFAULT_REFERENCE : this.projectId;
        final String correctStartDate = (this.startDate == null)
                ? IS_NOT_STARTED
                : this.startDate.format(DATE_TIME_FORMATTER);
        final String correctEndDate = (this.endDate == null)
                ? IS_NOT_ENDED
                : this.endDate.format(DATE_TIME_FORMATTER);
        return this.name +
                ": [ID: " + this.id +
                "; Description: " + this.description +
                "; Status: " + this.status.getDisplayName() +
                "; Project ID: " + correctProjectId +
                "; Start Date: " + correctStartDate +
                "; End Date: " + correctEndDate + "]";
    }

}
