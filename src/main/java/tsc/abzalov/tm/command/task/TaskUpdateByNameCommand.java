package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public class TaskUpdateByNameCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "update-task-by-name";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update task by name.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT TASK BY NAME\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            final String taskName = inputName();
            final String taskDescription = inputDescription();
            System.out.println();

            final Task task = taskService.updateTaskByName(taskName, taskDescription);
            if (task == null) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }

            System.out.println("Task was successfully updated.");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
