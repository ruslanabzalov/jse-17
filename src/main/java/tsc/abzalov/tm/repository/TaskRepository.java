package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.enumeration.Status.*;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public int size() {
        return tasks.size();
    }

    @Override
    public int indexOf(@NotNull final Task task) {
        return tasks.indexOf(task);
    }

    @Override
    public void createTask(@NotNull final String id, @NotNull final String description) {
        tasks.add(new Task(id, description));
    }

    @Override
    public void addTaskToProjectById(@NotNull final String projectId, @NotNull final String taskId) {
        for (final Task task : tasks) {
            if (task.getId().equals(taskId)) {
                task.setProjectId(projectId);
                return;
            }
        }
    }

    @Override
    @NotNull
    public List<Task> findAllTasks() {
        return tasks;
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String id) {
        for (final Task task : tasks)
            if (task.getId().equals(id)) return task;
        return null;
    }

    @Override
    @Nullable
    public Task findTaskByIndex(final int index) {
        return tasks.get(index);
    }

    @Override
    @Nullable
    public Task findTaskByName(@NotNull final String name) {
        for (final Task task : tasks)
            if (task.getName().equals(name)) return task;
        return null;
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(@NotNull final String projectId) {
        return tasks.stream()
                .filter(task -> isProjectTaskExist(projectId, task))
                .collect(Collectors.toList());
    }


    @Override
    @Nullable
    public Task updateTaskById(@NotNull final String id, @NotNull final String name,
                                 @NotNull final String description) {
        final Task task = findTaskById(id);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByIndex(final int index, @NotNull final String name, @NotNull final String description) {
        final Task task = findTaskByIndex(index);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByName(@NotNull final String name, @NotNull final String description) {
        final Task task = findTaskByName(name);
        if (task == null) return null;

        task.setDescription(description);
        return task;
    }

    @Override
    public void deleteAllTasks() {
        tasks.clear();
    }

    @Override
    public void deleteTaskById(@NotNull final String id) {
        final Task task = findTaskById(id);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByIndex(final int index) {
        final Task task = tasks.get(index);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByName(@NotNull final String name) {
        final Task task = findTaskByName(name);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteProjectTasksById(@NotNull final String projectId) {
        tasks.removeAll(findProjectTasksById(projectId));
    }

    @Override
    public void deleteProjectTaskById(@NotNull final String taskId) {
        for (final Task task : tasks) {
            if (task.getId().equals(taskId)) {
                if (task.getProjectId() != null) {
                    task.setProjectId(null);
                }
            }
        }
    }

    @Override
    public Task startTaskById(@NotNull final String id) {
        final Task task = findTaskById(id);
        if (task == null || task.getStatus() != TODO) return null;

        task.setStatus(IN_PROGRESS);
        task.setStartDate(LocalDateTime.now());
        return task;
    }

    @Override
    public Task endTaskById(@NotNull final String id) {
        final Task task = findTaskById(id);
        if (task == null || task.getStatus() != IN_PROGRESS) return null;

        task.setStatus(DONE);
        task.setEndDate(LocalDateTime.now());
        return task;
    }

    private boolean isProjectTaskExist(@NotNull final String taskId, @NotNull final Task task) {
        final String projectTaskId = task.getProjectId();
        if (projectTaskId == null) return false;
        return projectTaskId.equals(taskId);
    }
    
}
