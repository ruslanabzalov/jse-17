package tsc.abzalov.tm.command.interaction;

import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public class CommandShowProjectTasks extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-project-tasks";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project tasks.";
    }

    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW PROJECT TASKS\n");

        final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        if (projectTasksService.hasData()) {
            System.out.println("Project");
            String projectId = InputUtil.inputId();
            System.out.println();

            List<Task> foundedTasks = projectTasksService.findProjectTasksById(projectId);
            if (foundedTasks.size() == 0) {
                System.out.println("Tasks list is empty.\n");
                return;
            }

            foundedTasks.forEach(task -> System.out.println((projectTasksService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("One of the lists is empty!");
    }

}
