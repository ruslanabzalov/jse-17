package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;
import static tsc.abzalov.tm.util.Formatter.formatBytes;

public class SystemInfoCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "info";
    }

    @Override
    public String getCommandArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Shows CPU and RAM info.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        final int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Max Memory: " + formatBytes(maxMemory));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total Memory: " + formatBytes(totalMemory));

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free Memory: " + formatBytes(freeMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used Memory: " + formatBytes(usedMemory) + "\n");
    }

}
