package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemArgumentsCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "arguments";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all available arguments.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        serviceLocator.getCommandService().getCommandArguments().forEach(System.out::println);
        System.out.println();
    }

}
