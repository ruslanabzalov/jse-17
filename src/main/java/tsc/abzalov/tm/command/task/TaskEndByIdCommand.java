package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public class TaskEndByIdCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "end-task-by-id";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "End task by id.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("END TASK BY ID\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            final Task task = taskService.endTaskById(inputId());
            if (task == null) {
                System.out.println(
                        "Task was not ended!" +
                        "Please, check that task exists or it has correct status.\n"
                );
                return;
            }

            System.out.println("Task was successfully ended.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
