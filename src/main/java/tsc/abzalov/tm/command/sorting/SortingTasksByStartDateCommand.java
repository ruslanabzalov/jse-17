package tsc.abzalov.tm.command.sorting;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public class SortingTasksByStartDateCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "sort-tasks-by-start-date";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Sort tasks by start date.";
    }

    @Override
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL TASKS LIST SORTED BY START DATE\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            final List<Task> tasks = taskService.sortTasksByStartDate();
            tasks.forEach(task -> System.out.println((taskService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
