package tsc.abzalov.tm.command.interaction;

import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public class CommandDeleteProjectTask extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-project-task";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete project task.";
    }

    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE TASK FROM PROJECT\n");
        final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        if (projectTasksService.hasData()) {
            System.out.println("Task");
            final String taskId = InputUtil.inputId();
            System.out.println();

            if (projectTasksService.findTaskById(taskId) == null) {
                System.out.println("Task was not found.\n");
                return;
            }

            projectTasksService.deleteProjectTaskById(taskId);
            System.out.println("Task was deleted from the project.\n");
            return;
        }

        System.out.println("One of the lists is empty!");
    }

}
