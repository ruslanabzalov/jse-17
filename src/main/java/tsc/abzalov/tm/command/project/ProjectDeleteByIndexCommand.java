package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

public class ProjectDeleteByIndexCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-project-by-index";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete project by index.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE PROJECT BY INDEX\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            projectService.deleteProjectByIndex(inputIndex());
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
