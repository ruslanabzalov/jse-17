package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static tsc.abzalov.tm.enumeration.Status.*;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public int size() {
        return projects.size();
    }

    @Override
    public int indexOf(@NotNull final Project project) {
        return projects.indexOf(project);
    }

    @Override
    public void createProject(@NotNull final String id, @NotNull final String description) {
        projects.add(new Project(id, description));
    }

    @Override
    @NotNull
    public List<Project> findAllProjects() {
        return projects;
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String id) {
        for (final Project project : projects)
            if (project.getId().equals(id)) return project;
        return null;
    }

    @Override
    @Nullable
    public Project findProjectByIndex(final int index) {
        return projects.get(index);
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull final String name) {
        for (final Project project : projects)
            if (project.getName().equals(name)) return project;
        return null;
    }

    @Override
    @Nullable
    public Project updateProjectById(@NotNull final String id, @NotNull final String name,
                                    @NotNull final String description) {
        final Project project = findProjectById(id);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(final int index, @NotNull final String name,
                                       @NotNull final String description) {
        final Project project = findProjectByIndex(index);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByName(@NotNull final String name, @NotNull final String description) {
        final Project project = findProjectByName(name);
        if (project == null) return null;

        project.setDescription(description);
        return project;
    }

    @Override
    public void deleteAllProjects() {
        projects.clear();
    }

    @Override
    public void deleteProjectById(@NotNull final String id) {
        final Project project = findProjectById(id);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByIndex(final int index) {
        final Project project = projects.get(index);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByName(@NotNull final String name) {
        final Project project = findProjectByName(name);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public Project startProjectById(@NotNull final String id) {
        final Project project = findProjectById(id);
        if (project == null || project.getStatus() != TODO) return null;

        project.setStatus(IN_PROGRESS);
        project.setStartDate(LocalDateTime.now());
        return project;
    }

    @Override
    public Project endProjectById(@NotNull final String id) {
        final Project project = findProjectById(id);
        if (project == null || project.getStatus() != IN_PROGRESS) return null;

        project.setStatus(DONE);
        project.setEndDate(LocalDateTime.now());
        return project;
    }

}
