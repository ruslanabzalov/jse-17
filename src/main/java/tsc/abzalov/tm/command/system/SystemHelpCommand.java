package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.ArrayList;
import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemHelpCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String getCommandArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Shows all available commands.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        List<AbstractCommand> commandsList = new ArrayList<>(serviceLocator.getCommandService().getCommands());
        commandsList.sort((firstCommand, secondCommand) -> {
            final int firstOrdinal = firstCommand.getCommandType().ordinal();
            final int secondOrdinal = secondCommand.getCommandType().ordinal();
            if (firstOrdinal == secondOrdinal) return 0;
            if (firstOrdinal > secondOrdinal) return firstOrdinal - secondOrdinal;
            return secondOrdinal - firstOrdinal;
        });
        String helpMessage = formatHelpMessage(commandsList);
        System.out.print(helpMessage);
    }

    @NotNull
    private String formatHelpMessage(@NotNull List<AbstractCommand> commands) {
        final StringBuilder builder = new StringBuilder();

        for (final AbstractCommand command : commands)
            builder.append(command.toString()).append("\n");

        return builder.append("\n").toString();
    }

}
