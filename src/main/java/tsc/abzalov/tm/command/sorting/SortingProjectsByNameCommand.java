package tsc.abzalov.tm.command.sorting;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public class SortingProjectsByNameCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "sort-projects-by-name";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Sort projects by name.";
    }

    @Override
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL PROJECTS LIST SORTED BY NAME\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final List<Project> projects = projectService.sortProjectsByName();
            projects.forEach(project -> System.out.println((projectService.indexOf(project) + 1) + ". " + project));
            System.out.println();
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
