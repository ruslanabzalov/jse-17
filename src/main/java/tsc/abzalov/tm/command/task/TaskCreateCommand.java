package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "create-task";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create task.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("TASK CREATION\n");
        final String taskName = inputName();
        final String taskDescription = inputDescription();
        serviceLocator.getTaskService().createTask(taskName, taskDescription);

        System.out.println("Task was successfully created.\n");
    }

}
