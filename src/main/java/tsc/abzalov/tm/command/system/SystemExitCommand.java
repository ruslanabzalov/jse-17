package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemExitCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "exit";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Shutdowns application.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("Application Is Closing...");
        System.exit(0);
    }

}
