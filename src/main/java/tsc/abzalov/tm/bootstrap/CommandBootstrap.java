package tsc.abzalov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.command.interaction.CommandAddTaskToProject;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTask;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTasks;
import tsc.abzalov.tm.command.interaction.CommandShowProjectTasks;
import tsc.abzalov.tm.command.project.*;
import tsc.abzalov.tm.command.sorting.*;
import tsc.abzalov.tm.command.task.*;
import tsc.abzalov.tm.command.system.*;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.service.*;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

public final class CommandBootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        initCommand(new SystemAboutCommand());
        initCommand(new SystemArgumentsCommand());
        initCommand(new SystemCommandsCommand());
        initCommand(new SystemExitCommand());
        initCommand(new SystemHelpCommand());
        initCommand(new SystemInfoCommand());
        initCommand(new SystemVersionCommand());
        initCommand(new ProjectCreateCommand());
        initCommand(new ProjectDeleteAllCommand());
        initCommand(new ProjectDeleteByIdCommand());
        initCommand(new ProjectDeleteByIndexCommand());
        initCommand(new ProjectDeleteByNameCommand());
        initCommand(new ProjectEndByIdCommand());
        initCommand(new ProjectShowAllCommand());
        initCommand(new ProjectShowByIdCommand());
        initCommand(new ProjectShowByIndexCommand());
        initCommand(new ProjectShowByNameCommand());
        initCommand(new ProjectStartByIdCommand());
        initCommand(new ProjectUpdateByIdCommand());
        initCommand(new ProjectUpdateByIndexCommand());
        initCommand(new ProjectUpdateByNameCommand());
        initCommand(new TaskCreateCommand());
        initCommand(new TaskDeleteAllCommand());
        initCommand(new TaskDeleteByIdCommand());
        initCommand(new TaskDeleteByIndexCommand());
        initCommand(new TaskDeleteByNameCommand());
        initCommand(new TaskEndByIdCommand());
        initCommand(new TaskShowAllCommand());
        initCommand(new TaskShowByIdCommand());
        initCommand(new TaskShowByIndexCommand());
        initCommand(new TaskShowByNameCommand());
        initCommand(new TaskStartByIdCommand());
        initCommand(new TaskUpdateByIdCommand());
        initCommand(new TaskUpdateByIndexCommand());
        initCommand(new TaskUpdateByNameCommand());
        initCommand(new CommandAddTaskToProject());
        initCommand(new CommandDeleteProjectTask());
        initCommand(new CommandDeleteProjectTasks());
        initCommand(new CommandShowProjectTasks());
        initCommand(new SortingProjectsByEndDateCommand());
        initCommand(new SortingProjectsByNameCommand());
        initCommand(new SortingProjectsByStartDateCommand());
        initCommand(new SortingProjectsByStatusCommand());
        initCommand(new SortingTasksByEndDateCommand());
        initCommand(new SortingTasksByNameCommand());
        initCommand(new SortingTasksByStartDateCommand());
        initCommand(new SortingTasksByStatusCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public void run(@NotNull String... args) {
        System.out.println();
        try {
            if (areArgExists(args)) return;
        } catch (Exception exception) {
            loggerService.error(exception);
            return;
        }

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");

        String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();
            if (isEmpty(commandName)) continue;
            try {
                loggerService.command(commandName);
                commandService.getCommandByName(commandName).execute();
            }
            catch (Exception exception) {
                loggerService.error(exception);
            }
        }
    }

    private void initCommand(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean areArgExists(@NotNull String... args) throws Exception {
        if (isEmpty(args)) return false;

        final String arg = args[0];
        if (isEmpty(arg)) return false;

        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
