package tsc.abzalov.tm.command.system;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public class SystemCommandsCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "command";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all available commands.";
    }

    @Override
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        serviceLocator.getCommandService().getCommandNames().forEach(System.out::println);
        System.out.println();
    }

}
