package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

public class TaskDeleteByIndexCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-task-by-index";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete task by index.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE TASK BY INDEX\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            taskService.deleteTaskByIndex(inputIndex());
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
