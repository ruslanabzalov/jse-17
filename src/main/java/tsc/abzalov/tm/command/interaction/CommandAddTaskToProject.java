package tsc.abzalov.tm.command.interaction;

import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public class CommandAddTaskToProject extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "add-task-to-project";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Add task to project.";
    }

    @Override
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ADD TASK TO PROJECT\n");
        System.out.println("Project");
        final String projectId = InputUtil.inputId();
        System.out.println();

        final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        if (projectTasksService.findProjectById(projectId) == null) {
            System.out.println("Project was not found.\n");
            return;
        }

        System.out.println("Task");
        final String taskId = InputUtil.inputId();
        System.out.println();

        if (projectTasksService.findTaskById(taskId) == null) {
            System.out.println("Task was not found.\n");
            return;
        }

        projectTasksService.addTaskToProjectById(projectId, taskId);
        System.out.println("Task was successfully added to the project.\n");
    }

}
