package tsc.abzalov.tm.enumeration;

public enum CommandType {

    SYSTEM_COMMAND("System Command"),
    PROJECT_COMMAND("Project Command"),
    TASK_COMMAND("Task Command"),
    INTERACTION_COMMAND("Interaction Command"),
    SORTING_COMMAND("Sorting Command");

    private final String name;

    CommandType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
