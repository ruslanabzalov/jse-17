package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    int size();

    int indexOf(Task task);

    void createTask(String name, String description) throws Exception;

    List<Task> findAllTasks();

    Task findTaskById(String id) throws Exception;

    Task findTaskByIndex(int index) throws Exception;

    Task findTaskByName(String name) throws Exception;

    Task updateTaskById(String id, String name, String description) throws Exception;

    Task updateTaskByIndex(int index, String name, String description) throws Exception;

    Task updateTaskByName(String name, String description) throws Exception;

    void deleteAllTasks();

    void deleteTaskById(String id) throws Exception;

    void deleteTaskByIndex(int index) throws Exception;

    void deleteTaskByName(String name) throws Exception;

    Task startTaskById(String id) throws Exception;

    Task endTaskById(String id) throws Exception;

    List<Task> sortTasksByName();

    List<Task> sortTasksByStartDate();

    List<Task> sortTasksByEndDate();

    List<Task> sortTasksByStatus();
    
}
