package tsc.abzalov.tm.command;

import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.enumeration.CommandType;

import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommandName();

    public abstract String getCommandArgument();

    public abstract String getDescription();

    public abstract CommandType getCommandType();

    public abstract void execute() throws Exception ;

    @Override
    public String toString() {
        final String correctArg = (isBlank(getCommandArgument())) ? ": " : " [" + getCommandArgument() + "]: ";
        return getCommandName() + correctArg + getDescription();
    }

}
