package tsc.abzalov.tm.exception.general;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException(@NotNull String command) {
        super("Command \"" + command + "\" is not available!\n" + "Please, use \"help\" command.");
    }

}
