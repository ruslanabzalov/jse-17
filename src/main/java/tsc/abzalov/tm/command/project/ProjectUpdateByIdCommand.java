package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "update-project-by-id";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by id.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT PROJECT BY ID\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final String projectId = inputId();
            final String projectName = inputName();
            final String projectDescription = inputDescription();
            System.out.println();

            final Project project = projectService.updateProjectById(projectId, projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }

            System.out.println("Project was successfully updated.");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
