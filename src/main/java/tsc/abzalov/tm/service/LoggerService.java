package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

import static java.util.logging.Logger.getGlobal;
import static java.util.logging.Logger.getLogger;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class LoggerService implements ILoggerService {

    private static final String MESSAGES = "MESSAGES";

    private static final String COMMANDS = "COMMANDS";

    private static final String ERRORS = "ERRORS";

    private static final String COMMANDS_LOG_FILE_PATH = "logs/access.log";

    private static final String ERRORS_LOG_FILE_PATH = "logs/errors.log";

    @NotNull
    private final Logger messages = getLogger(MESSAGES);

    @NotNull
    private final Logger commands = getLogger(COMMANDS);

    @NotNull
    private final Logger error = getLogger(ERRORS);

    @NotNull
    private final Logger root = getGlobal();

    public LoggerService() {
        initLogger(messages, null);
        initLogger(commands, COMMANDS_LOG_FILE_PATH);
        initLogger(error, ERRORS_LOG_FILE_PATH);
    }

    @Override
    public void info(@NotNull String message) {
        if (isBlank(message)) return;
        messages.info(message);
    }

    @Override
    public void command(@NotNull String cmd) {
        if (isBlank(cmd)) return;
        commands.info("Execution of command \"" + cmd + "\"");
    }

    @Override
    public void error(@NotNull Exception exception) {
        if (exception == null) return;
        error.log(Level.SEVERE, exception.getMessage(), exception);
    }

    private void initLogger(@NotNull Logger logger, @Nullable String filePath) {
        if (logger == null) return;

        logger.setUseParentHandlers(false);
        logger.addHandler(getConsoleHandler());

        if (isBlank(filePath)) return;

        try {
            logger.addHandler(new FileHandler(filePath));
        } catch (IOException exception) {
            root.severe(exception.getMessage());
        }
    }

    @NotNull
    private Handler getConsoleHandler() {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return consoleHandler;
    }

}
