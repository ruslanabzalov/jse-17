package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

public class TaskDeleteAllCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-all-tasks";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete all tasks.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("TASKS DELETION\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final boolean areTasksExist = taskService.size() != 0;
        if (areTasksExist) {
            taskService.deleteAllTasks();
            System.out.println("Tasks were deleted.\n");
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
