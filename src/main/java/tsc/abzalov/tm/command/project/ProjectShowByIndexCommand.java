package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

public class ProjectShowByIndexCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-project-by-index";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("FIND PROJECT BY INDEX\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final boolean areProjectsExist = projectService.size() != 0;
        if (areProjectsExist) {
            final int projectIndex = inputIndex();
            System.out.println();

            final Project project = projectService.findProjectByIndex(projectIndex);
            if (project == null) {
                System.out.println("Searched project was not found.\n");
                return;
            }

            System.out.println((projectService.indexOf(project) + 1) + ". " + project + "\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
