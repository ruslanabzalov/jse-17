package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.*;

import static org.apache.commons.lang3.StringUtils.isBlank;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        return getNames(getCommands());
    }

    @Override
    @NotNull
    public Collection<String> getCommandArguments() {
        return getNames(arguments.values());
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getArgumentByName(@NotNull String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        final String name = command.getCommandName();
        final String argument = command.getCommandArgument();
        if (!isBlank(name)) commands.put(name, command);
        if (!isBlank(argument)) arguments.put(name, command);
    }

    @NotNull
    private List<String> getNames(@NotNull Collection<AbstractCommand> commands) {
        List<String> names = new ArrayList<>();
        for (final AbstractCommand command : commands) {
            final String name = command.getCommandArgument();
            if (isBlank(name)) continue;
            names.add(name);
        }
        return names;
    }

}
