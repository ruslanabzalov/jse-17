package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    int size();

    int indexOf(Project project);

    void createProject(String name, String description) throws Exception;

    List<Project> findAllProjects();

    Project findProjectById(String id) throws Exception;

    Project findProjectByIndex(int index) throws Exception;

    Project findProjectByName(String name) throws Exception;

    Project updateProjectById(String id, String name, String description) throws Exception;

    Project updateProjectByIndex(int index, String name, String description) throws Exception;

    Project updateProjectByName(String name, String description) throws Exception;

    void deleteAllProjects();

    void deleteProjectById(String id) throws Exception;

    void deleteProjectByIndex(int index) throws Exception;

    void deleteProjectByName(String name) throws Exception;

    Project startProjectById(String id) throws Exception;

    Project endProjectById(String id) throws Exception;

    List<Project> sortProjectsByName();

    List<Project> sortProjectsByStartDate();

    List<Project> sortProjectsByEndDate();

    List<Project> sortProjectsByStatus();
    
}
