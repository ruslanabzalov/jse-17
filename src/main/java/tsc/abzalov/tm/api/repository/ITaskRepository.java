package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    int size();

    int indexOf(Task task);

    void createTask(String id, String description);

    void addTaskToProjectById(String projectId, String taskId);

    List<Task> findAllTasks();

    Task findTaskById(String id);

    Task findTaskByIndex(int index);

    Task findTaskByName(String name);

    List<Task> findProjectTasksById(String projectId);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(int index, String name, String description);

    Task updateTaskByName(String name, String description);

    void deleteAllTasks();

    void deleteTaskById(String id);

    void deleteTaskByIndex(int index);

    void deleteTaskByName(String name);

    void deleteProjectTasksById(String taskId);

    void deleteProjectTaskById(String taskId);

    Task startTaskById(String id);

    Task endTaskById(String id);
    
}
