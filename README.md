# JSE TASK MANAGER

## About Application
Task Manager Application

## About Developer
* **Full Name**: Ruslan Abzalov
* **E-Mail**: ruslanonetwo@gmail.com
* **Company**: Technoserv Consulting

## Hardware
### Work Laptop
* **CPU**: Intel Core i5-7200U @ 2.50 GHz x 4
* **RAM**: 8 GB
* **Disk Capacity**: SSD 256 GB

### Home Laptop
* **CPU**: Intel Core i7-7500U @ 2.70 GHz x 4
* **RAM**: 8 GB
* **Disk Capacity**: SSD 256 GB

## Software
### Work Laptop
* **OS Name**: Windows 10 Enterprise LTSC 1809
* **OS Type**: 64-bit
* **JDK**: 1.8+

### Home Laptop
* **OS Name**: Ubuntu 20.04.1 LTS
* **OS Type**: 64-bit
* **JDK**: 1.8+

## How To Build
```
$ mvn clean compile assembly:single
```
## How To Run
```
$ java -jar target/task-manager-1.0-jar-with-dependencies.jar
```

## Tasks result
* **JSE-00**: [jse-00](https://drive.google.com/drive/folders/1vwK52B_H550TXXs2pLqFJfb83yswBuj4?usp=sharing)
* **JSE-01**: [jse-01-task-manager-result.png](https://drive.google.com/file/d/1w3qYGjnhBTM0jpmxhF_G_OPoruwfJD99/view?usp=sharing)
* **JSE-02**: [jse-02-task-manager-result.png](https://drive.google.com/file/d/1h1hVvMiSoqK0iVvXO9F1ESBWBAJN5K8H/view?usp=sharing)
* **JSE-03**: [jse-03](https://drive.google.com/drive/folders/14YWFA2tSnaPmDJH-tx6XaPdG_ElknKYD?usp=sharing)
* **JSE-04**: [jse-04](https://drive.google.com/drive/folders/1PSmSwD7qiUNZlO_5cA5DgIXau0NPX1SG?usp=sharing)
* **JSE-05**: [jse-05](https://drive.google.com/drive/folders/1jLZviggFBpyeQBGrD8mLh0zXNoVc3ob7?usp=sharing)
* **JSE-06**: [jse-06](https://drive.google.com/drive/folders/1LzHcWtbE9folIBtpN89YllMR1dbarbVc?usp=sharing)
* **JSE-07**: [jse-07](https://drive.google.com/drive/folders/1wkPvDiUENQDdC45F4vbRw2NagDSP6NsQ?usp=sharing)
* **JSE-08**: [jse-08](https://drive.google.com/drive/folders/1WKYB-6hmNcAgUzt2r5b_TG65l-ZXxYuG?usp=sharing)
* **JSE-09**: [jse-09](https://drive.google.com/drive/folders/1NTER8GgpxVTCnHijiqeMMcgpikVFz-qm?usp=sharing)
* **JSE-10**: [jse-10](https://drive.google.com/drive/folders/1MP8Yfb2w3ArMKq799QXFyWqNqtp1_WsP?usp=sharing)
* **JSE-11**: [jse-11](https://drive.google.com/drive/folders/1BlkEHdl9wIrUPxI61EG_Jfj0MfI_F9Ee?usp=sharing)
* **JSE-12**: [jse-12](https://drive.google.com/drive/folders/18qWYCQiGiNfjS3PI0GfzWfOhA8UjCoL4?usp=sharing)
* **JSE-13**: [jse-13](https://drive.google.com/drive/folders/1pYL0YqEhq7L9DRQ6IAMhOAy6AozoHLfJ?usp=sharing)
* **JSE-14**: [jse-14](https://drive.google.com/drive/folders/1L5QsIeTWXmEDRo64g3WYZx8jSguFjuVe?usp=sharing)
* **JSE-15**: [jse-15](https://drive.google.com/drive/folders/1M-MJ9UYyLtYmOtd1NmIWcC_O5RJswCON?usp=sharing)
* **JSE-16**: [jse-16](https://drive.google.com/drive/folders/1Q31gn85qW-wfQ7VMzXYqJQvVhv_dI_1o?usp=sharing)
* **JSE-17**: [jse-17](https://drive.google.com/drive/folders/1uI2VZiJ4geIP-6knzJgprHhDExoUypP6?usp=sharing)