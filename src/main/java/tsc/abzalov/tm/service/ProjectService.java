package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.IncorrectIndexException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public int size() {
        return projectRepository.size();
    }

    @Override
    public int indexOf(@NotNull final Project project) {
        return projectRepository.indexOf(project);
    }

    @Override
    public void createProject(@NotNull final String name, @NotNull String description) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        projectRepository.createProject(name, description);
    }

    @Override
    @NotNull
    public List<Project> findAllProjects() {
        return sortProjectsByStartDate();
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        return projectRepository.findProjectById(id);
    }

    @Override
    @Nullable
    public Project findProjectByIndex(final int index) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (isIndexCorrect) return projectRepository.findProjectByIndex(index - 1);
        throw new IncorrectIndexException(index);
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull final String name) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        return projectRepository.findProjectByName(name);
    }

    @Override
    @Nullable
    public Project updateProjectById(@NotNull final String id, @NotNull final String name,
                                     @NotNull String description) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectById(id, name, description);
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(final int index, @NotNull String name,
                                        @NotNull String description)  throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (!isIndexCorrect) throw new IncorrectIndexException(index);
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectByIndex(index - 1, name, description);
    }

    @Override
    @Nullable
    public Project updateProjectByName(@NotNull final String name, @NotNull String description) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectByName(name, description);
    }

    @Override
    public void deleteAllProjects() {
        projectRepository.deleteAllProjects();
    }

    @Override
    public void deleteProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        projectRepository.deleteProjectById(id);
    }

    @Override
    public void deleteProjectByIndex(final int index) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (!isIndexCorrect) throw new IncorrectIndexException(index);
        projectRepository.deleteProjectByIndex(index - 1);
    }

    @Override
    public void deleteProjectByName(@NotNull final String name) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        projectRepository.deleteProjectByName(name);
    }

    @Override
    @Nullable
    public Project startProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        return projectRepository.startProjectById(id);
    }

    @Override
    @Nullable
    public Project endProjectById(@NotNull final String id) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        return projectRepository.endProjectById(id);
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByName() {
        final List<Project> projects = projectRepository.findAllProjects();

        projects.sort((firstProject, secondProject) -> {
            final String firstProjectName = firstProject.getName();
            final String secondProjectName = secondProject.getName();
            return firstProjectName.compareTo(secondProjectName);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByStartDate() {
        final List<Project> projects = projectRepository.findAllProjects();

        projects.sort((firstProject, secondProject) -> {
            final LocalDateTime firstProjectStartDate = firstProject.getStartDate();
            final LocalDateTime secondProjectStartDate = secondProject.getStartDate();
            if (anyNull(firstProjectStartDate, secondProjectStartDate)) return 0;
            return firstProjectStartDate.compareTo(secondProjectStartDate);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByEndDate() {
        final List<Project> projects = projectRepository.findAllProjects();

        projects.sort((firstProject, secondProject) -> {
            final LocalDateTime firstProjectEndDate = firstProject.getEndDate();
            final LocalDateTime secondProjectEndDate = secondProject.getEndDate();
            if (anyNull(firstProjectEndDate, secondProjectEndDate)) return 0;
            return firstProjectEndDate.compareTo(secondProjectEndDate);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByStatus() {
        final List<Project> projects = projectRepository.findAllProjects();

        projects.sort((firstProject, secondProject) -> {
            final Status firstProjectStatus = firstProject.getStatus();
            final Status secondProjectStatus = secondProject.getStatus();
            return firstProjectStatus.compareTo(secondProjectStatus);
        });

        return projects;
    }
    
}
